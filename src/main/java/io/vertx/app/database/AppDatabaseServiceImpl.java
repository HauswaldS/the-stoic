package io.vertx.app.database;

import io.reactivex.Observable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.auth.mongo.MongoAuth;
import io.vertx.reactivex.ext.mongo.MongoClient;

import java.util.ArrayList;

public class AppDatabaseServiceImpl implements AppDatabaseService {
    private static Logger LOGGER = LoggerFactory.getLogger(AppDatabaseServiceImpl.class);
    private static final String CONFIG_APPDB_COLLECTIONS = "appdb.collections";
    private final MongoClient mongoClient;
    private final MongoAuth authProvider;


    AppDatabaseServiceImpl(MongoClient mongoClient, MongoAuth authProvider, JsonObject config, Handler<AsyncResult<AppDatabaseService>> readyHandler) {
        this.mongoClient = mongoClient;
        this.authProvider = authProvider;
        String[] collections = config.getString(CONFIG_APPDB_COLLECTIONS).split(",");

        //Initializing MongoDB collections
        this.mongoClient.rxGetCollections()
                .map(currentCollections -> {
                    ArrayList<String> missingCollections = new ArrayList<>();
                    for (String collectionName : collections) {
                        if (!currentCollections.contains(collectionName)) {
                            missingCollections.add(collectionName);
                        }
                    }
                    return missingCollections;
                })
                .flatMapObservable(missingCollections -> Observable.fromIterable(missingCollections))
                .flatMapCompletable(collectionName -> mongoClient.rxCreateCollection(collectionName))
                .subscribe(
                        () -> {
                            LOGGER.info("Collections created and MongoDB client instantiated successfully.");
                            readyHandler.handle(Future.succeededFuture(this));
                        },
                        err -> {
                            LOGGER.error("Collections creation failed, aborting MongoDB client instantation:", err);
                            readyHandler.handle(Future.failedFuture(err));
                        }
                );
    }

    //AUTHORS
    @Override
    public AppDatabaseService getAllAuthors(JsonObject query, Handler<AsyncResult<String>> readyHandler) {
        mongoClient.rxFind("author", query)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res.toString())),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService getAuthor(String id, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxFindOne("author", new JsonObject().put("_id", id), null)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res)),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService createAuthor(JsonObject author, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxSave("author", author)
                .map(authorId -> getAuthor(authorId, readyHandler))
                .subscribe();
        return this;
    }

    @Override
    public AppDatabaseService updateAuthor(JsonObject author, String id, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxFindOneAndReplace("author", new JsonObject().put("_id", id), author)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res)),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService deleteAuthor(String id, Handler<AsyncResult<Void>> readyHandler) {
        mongoClient.rxFindOneAndDelete("author", new JsonObject().put("_id", id))
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture()),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    //QUOTES
    @Override
    public AppDatabaseService getAllQuotes(JsonObject query, Handler<AsyncResult<String>> readyHandler) {
        mongoClient.rxFind("quote", query)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res.toString())),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService getQuote(String id, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxFindOne("quote", new JsonObject().put("_id", id), null)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res)),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService createQuote(JsonObject quote, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxSave("quote", quote)
                .map(quoteId -> getQuote(quoteId, readyHandler))
                .subscribe();
        return this;
    }

    @Override
    public AppDatabaseService updateQuote(JsonObject quote, String id, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxFindOneAndReplace("quote", new JsonObject().put("_id", id), null)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res)),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService deleteQuote(String id, Handler<AsyncResult<Void>> readyHandler) {
        mongoClient.rxFindOneAndDelete("quote", new JsonObject().put("_id", id))
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture()),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    //USERS
    @Override
    public AppDatabaseService getUser(String id, Handler<AsyncResult<JsonObject>> readyHandler) {
        mongoClient.rxFindOne("user", new JsonObject().put("_id", id), null)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res)),
                        err -> readyHandler.handle(Future.failedFuture(err.getMessage()))
                );
        return this;
    }

    @Override
    public AppDatabaseService createUser(JsonObject user, Handler<AsyncResult<JsonObject>> readyHandler) {
        ArrayList<String> roles = new ArrayList<>();
        ArrayList<String> permissions = new ArrayList<>();
        authProvider.rxInsertUser(
                user.getString("username"),
                user.getString("password"),
                roles,
                permissions)
                .map(userId -> getUser(userId, readyHandler))
                .subscribe();
        return this;
    }

    @Override
    public AppDatabaseService authenticateUser(JsonObject user, Handler<AsyncResult<String>> readyHandler) {
        authProvider.rxAuthenticate(user)
                .subscribe(
                        res -> readyHandler.handle(Future.succeededFuture(res.toString())),
                        err -> readyHandler.handle(Future.failedFuture(err.getCause()))
                );
        return this;
    }
}
