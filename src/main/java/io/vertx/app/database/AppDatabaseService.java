package io.vertx.app.database;

import io.vertx.codegen.annotations.Fluent;
import io.vertx.codegen.annotations.GenIgnore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.auth.mongo.MongoAuth;
import io.vertx.reactivex.ext.mongo.MongoClient;

@ProxyGen
@VertxGen
public interface AppDatabaseService {

    @GenIgnore
    static AppDatabaseService create(MongoClient dbClient,
                                     MongoAuth authProvider,
                                     JsonObject config,
                                     Handler<AsyncResult<AppDatabaseService>> readyHandler) {
        return new AppDatabaseServiceImpl(dbClient, authProvider, config, readyHandler);
    }

    @GenIgnore
    static AppDatabaseService createProxy(Vertx vertx, String address) {
        return new AppDatabaseServiceVertxEBProxy(vertx, address);
    }


    //AUTHORS
    @Fluent
    AppDatabaseService getAllAuthors(JsonObject params, Handler<AsyncResult<String>> resultHandler);

    @Fluent
    AppDatabaseService getAuthor(String id, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService createAuthor(JsonObject author, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService updateAuthor(JsonObject author, String id, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService deleteAuthor(String id, Handler<AsyncResult<Void>> resultHandler);

    //QUOTES
    @Fluent
    AppDatabaseService getAllQuotes(JsonObject params, Handler<AsyncResult<String>> resultHandler);

    @Fluent
    AppDatabaseService getQuote(String id, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService createQuote(JsonObject quote, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService updateQuote(JsonObject quote, String id, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService deleteQuote(String id, Handler<AsyncResult<Void>> resultHandler);

    //USERS
    @Fluent
    AppDatabaseService authenticateUser(JsonObject user, Handler<AsyncResult<String>> resultHandler);

    @Fluent
    AppDatabaseService getUser(String id, Handler<AsyncResult<JsonObject>> resultHandler);

    @Fluent
    AppDatabaseService createUser(JsonObject user, Handler<AsyncResult<JsonObject>> resultHandler);
}
