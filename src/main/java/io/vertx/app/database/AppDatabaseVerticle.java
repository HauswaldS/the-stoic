package io.vertx.app.database;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.ext.auth.mongo.MongoAuth;
import io.vertx.reactivex.ext.mongo.MongoClient;
import io.vertx.serviceproxy.ServiceBinder;


public class AppDatabaseVerticle extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppDatabaseVerticle.class);
    private static final String CONFIG_APPDB_QUEUE = "appdb.queue";

    private MongoClient mongoClient;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        String appDbQueue = config().getString(CONFIG_APPDB_QUEUE);
        mongoClient = MongoClient.createShared(vertx, config());
        MongoAuth authProvider = MongoAuth.create(mongoClient, new JsonObject());
        AppDatabaseService.create(mongoClient, authProvider, config(), ready -> {
            if (ready.succeeded()) {
                ServiceBinder binder = new ServiceBinder(vertx.getDelegate());
                binder
                        .setAddress(appDbQueue)
                        .register(AppDatabaseService.class, ready.result());
                LOGGER.info("Database service instanciated successfully.");
                startFuture.complete();
            } else {
                LOGGER.error("Database service instantiation failed.", ready.cause());
                startFuture.fail(ready.cause());
            }
        });
    }
}

