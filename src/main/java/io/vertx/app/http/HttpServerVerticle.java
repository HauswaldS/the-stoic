package io.vertx.app.http;

import io.vertx.app.database.AppDatabaseService;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.web.Router;
import io.vertx.core.Future;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.ext.web.templ.freemarker.FreeMarkerTemplateEngine;

import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class HttpServerVerticle extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpServerVerticle.class);
    private static final String CONFIG_APPDB_QUEUE = "appdb.queue";
    private FreeMarkerTemplateEngine templateEngine;
    private JWTAuth jwtProvider;
    private static AppDatabaseService dbService;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        String appDbQueue = config().getString(CONFIG_APPDB_QUEUE, "appdb.queue");
        HttpServer httpServer = vertx.getDelegate().createHttpServer(new HttpServerOptions(config()));
        templateEngine = FreeMarkerTemplateEngine.create(vertx);
        dbService = AppDatabaseService.createProxy(vertx.getDelegate(), appDbQueue);
        jwtProvider = JWTAuth.create(vertx.getDelegate(), new JWTAuthOptions()
                .addPubSecKey(new PubSecKeyOptions()
                        .setAlgorithm("HS256")
                        .setPublicKey("some key")
                        .setSymmetric(true)));
        JWTAuthHandler AuthHandler = JWTAuthHandler.create(jwtProvider);

        Router router = Router.router(vertx.getDelegate());
        router.route().handler(CookieHandler.create());
        router.route().handler(BodyHandler.create());

        // SPA
        router.get("/").handler(this::indexHandler);

        //AUTHORS
        Router authorsRouter = Router.router(vertx.getDelegate());
        authorsRouter.get("/").handler(this::getAllAuthorsHandler);
        authorsRouter.get("/:id").handler(this::getAuthorHandler);
        authorsRouter.post("/").handler(AuthHandler).handler(this::createAuthorHandler);
        authorsRouter.put("/:id").handler(AuthHandler).handler(this::updateAuthorHandler);
        authorsRouter.delete("/:id").handler(AuthHandler).handler(this::deleteAuthorHandler);

        //QUOTES
        Router quotesRouter = Router.router(vertx.getDelegate());
        quotesRouter.get("/").handler(this::getAllQuotesHandler);
        quotesRouter.get("/:id").handler(this::getQuoteHandler);
        quotesRouter.post("/").handler(AuthHandler).handler(this::createQuoteHandler);
        quotesRouter.put("/:id").handler(AuthHandler).handler(this::updateQuoteHandler);
        quotesRouter.delete("/:id").handler(AuthHandler).handler(this::deleteQuoteHandler);

        //USERS
        Router usersRouter = Router.router(vertx.getDelegate());
        usersRouter.post("/").handler(this::createUserHandler);
        usersRouter.get("/:id").handler(this::getUserHandler);

        Router apiSubRouter = Router.router(vertx.getDelegate());
        apiSubRouter.get("/token").handler(this::getTokenHandler);
        apiSubRouter.mountSubRouter("/quotes", quotesRouter);
        apiSubRouter.mountSubRouter("/authors", authorsRouter);
        apiSubRouter.mountSubRouter("/users", usersRouter);
        router.mountSubRouter("/api", apiSubRouter);

        httpServer
                .requestHandler(router)
                .listen(ar -> {
                    if (ar.succeeded()) {
                        LOGGER.info("HTTP Server started on port: " + config().getInteger("port"));
                        startFuture.complete();
                    } else {
                        LOGGER.error("Could not start HTTP server", ar.cause());
                        startFuture.fail(ar.cause());
                    }
                });
    }

    //TODO: Set proper HTTP response status code
    //TODO: Refactor handler and auth provider to make use of RxJava

    private void indexHandler(RoutingContext routingContext) {
        String indexHTMLFilePath = Paths.get(System.getProperty("user.dir") + "/src/main/spa/index.ftl").toString();
        templateEngine.rxRender(new JsonObject(), indexHTMLFilePath)
                .map(html -> {
                            routingContext
                                    .response()
                                    .putHeader("Content-Type", "text/html")
                                    .end(html.toString());
                            return true;
                        }
                ).subscribe();
    }

    //AUTHORS
    private void getAllAuthorsHandler(RoutingContext routingContext) {
        JsonObject params = paramsListToJson(routingContext.request().params().entries());
        dbService.getAllAuthors(params, res -> {
            if (res.succeeded()) {
                LOGGER.info("GET /api/authors successful");
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result());
            } else {
                LOGGER.error("Failed to GET /api/authors", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void getAuthorHandler(RoutingContext routingContext) {
        String id = routingContext.request().params().get("id");
        dbService.getAuthor(id, res -> {
            if (res.succeeded()) {
                LOGGER.info("GET /api/authors/:id successful");
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result().encodePrettily());
            } else {
                LOGGER.error("Failed to GET /api/authors/:id", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void createAuthorHandler(RoutingContext routingContext) {
        JsonObject author = routingContext.getBodyAsJson();
        dbService.createAuthor(author, res -> {
            if (res.succeeded()) {
                LOGGER.info("POST /api/authors successful");
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result().encodePrettily());
            } else {
                LOGGER.error("Failed to POST /api/authors", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void updateAuthorHandler(RoutingContext routingContext) {
        JsonObject author = routingContext.getBodyAsJson();
        String id = routingContext.request().params().get("id");
        dbService.updateAuthor(author, id, res -> {
            if (res.succeeded()) {
                LOGGER.info("PUT /api/authors successful");
                routingContext.response()
                        .setStatusCode(202)
                        .putHeader("Content-Type", "application/json")
                        .end(author.encodePrettily());
            } else {
                LOGGER.error("Failed to PUT /api/authors", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void deleteAuthorHandler(RoutingContext routingContext) {
        String id = routingContext.request().params().get("id");
        dbService.deleteAuthor(id, res -> {
            if (res.succeeded()) {
                LOGGER.info("DELETE /api/authors successful");
                routingContext.response().setStatusCode(204).end();
            } else {
                LOGGER.error("Failed to DELETE /api/authors", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    //QUOTES
    private void getAllQuotesHandler(RoutingContext routingContext) {
        JsonObject params = paramsListToJson(routingContext.request().params().entries());
        dbService.getAllQuotes(params, res -> {
            if (res.succeeded()) {
                LOGGER.info("GET /api/quotes successful");
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result());
            } else {
                LOGGER.error("Failed to GET /api/quotes", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void getQuoteHandler(RoutingContext routingContext) {
        String id = routingContext.request().params().get("id");
        dbService.getQuote(id, res -> {
            if (res.succeeded()) {
                LOGGER.info("GET /api/quotes/:id successful");
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result().encodePrettily());

            } else {
                LOGGER.error("Failed to GET /api/quotes/:id", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void createQuoteHandler(RoutingContext routingContext) {
        JsonObject quote = routingContext.getBodyAsJson();
        dbService.createQuote(quote, res -> {
            if (res.succeeded()) {
                LOGGER.info("POST /api/quotes successful");
                routingContext.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result().encodePrettily());
            } else {
                LOGGER.error("Failed to GET /api/quotes", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void updateQuoteHandler(RoutingContext routingContext) {
        JsonObject quote = routingContext.getBodyAsJson();
        String id = routingContext.request().params().get("id");
        dbService.updateQuote(quote, id, res -> {
            if (res.succeeded()) {
                LOGGER.info("PUT /api/quotes successful");
                routingContext.response()
                        .setStatusCode(202)
                        .putHeader("Content-Type", "application/json")
                        .end(quote.encodePrettily());
            } else {
                LOGGER.error("Failed to PUT /api/quotes", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void deleteQuoteHandler(RoutingContext routingContext) {
        String id = routingContext.request().params().get("id");
        dbService.deleteQuote(id, res -> {
            if (res.succeeded()) {
                LOGGER.info("DELETE /api/quotes successful");
                routingContext.response().setStatusCode(204).end();
            } else {
                LOGGER.error("Failed to GET /api/quotes", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    //USERS
    private void getUserHandler(RoutingContext routingContext) {
        String userId = routingContext.request().params().get("id");
        dbService.getUser(userId, res -> {
            if (res.succeeded()) {
                LOGGER.info("GET /api/users/:id successful");
                routingContext.response()
                        .setStatusCode(202)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result().encodePrettily());
            } else {
                LOGGER.error("Failed to GET /api/users/:id ", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private void createUserHandler(RoutingContext routingContext) {
        JsonObject user = routingContext.getBodyAsJson();
        dbService.createUser(user, res -> {
            if (res.succeeded()) {
                LOGGER.info("POST /api/users successful");
                routingContext.response()
                        .setStatusCode(202)
                        .putHeader("Content-Type", "application/json")
                        .end(res.result().encodePrettily());
            } else {
                LOGGER.error("Failed to POST /api/users ", res.cause());
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    //AUTH TOKEN
    private void getTokenHandler(RoutingContext routingContext) {
        String username = routingContext.request().getParam("username");
        String password = routingContext.request().getParam("password");
        JsonObject credentials = new JsonObject()
                .put("username", username)
                .put("password", password);
        dbService.authenticateUser(credentials, res -> {
            if (res.succeeded()) {
                String token = jwtProvider.generateToken(new JsonObject().put("username", username));
                routingContext.response().setStatusCode(200).end(token);
            } else {
                routingContext.response().setStatusCode(500).end(res.cause().toString());
            }
        });
    }

    private static JsonObject paramsListToJson(List<Map.Entry<String, String>> params) {
        JsonObject jsonParams = new JsonObject();
        for (Map.Entry<String, String> param : params) {
            jsonParams.put(param.getKey(), param.getValue());
        }
        return jsonParams;
    }
}