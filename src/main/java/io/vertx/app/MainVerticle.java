package io.vertx.app;

import io.vertx.reactivex.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.core.AbstractVerticle;

import java.nio.file.Paths;

public class MainVerticle extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);


    @Override
    public void start(Future<Void> startFuture) throws Exception {
        String configFilePath = Paths.get(System.getProperty("user.dir") + "\\src\\main\\conf\\application-conf.json").toString();
        ConfigStoreOptions fileStore = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", configFilePath));
        ConfigRetrieverOptions options = new ConfigRetrieverOptions().addStore(fileStore);
        ConfigRetriever retriever = ConfigRetriever.create(vertx, options);

        retriever.getConfig(config -> {
            if (config.succeeded()) {
                LOGGER.info("Config successfully imported, starting to deploy verticles.");
                vertx.rxDeployVerticle(
                        "io.vertx.app.database.AppDatabaseVerticle",
                        new DeploymentOptions().setConfig(config.result()))
                        .flatMap(dbVerticleId ->
                                vertx.rxDeployVerticle(
                                        "io.vertx.app.http.HttpServerVerticle",
                                        new DeploymentOptions().setInstances(2).setConfig(config.result())
                                ))
                        .subscribe(httpVerticleId -> startFuture.completer(), startFuture::fail);
            } else {
                LOGGER.error("Failed to import config, aborting verticle deployement.", config.cause());
            }
        });
    }
}
